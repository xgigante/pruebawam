import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';

interface Result {
  title: string;
  text: string;
  img: string;
}

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.scss'],
})
export class StepComponent implements OnInit {
  @Input() storage;
  @Output() stepData = new EventEmitter<any>();
  step: number = 1;
  loading = false;
  result: Result;
  imgResult: string;
  name: string;
  surname: string;

  constructor(
    private apiService: ApiService,
    private toastrService: ToastrService
  ) {}

  ngOnInit(): void {}

  nextStep(): void {
    if (!this.controlInputs(this.name, this.surname)) {
      return;
    }
    this.step++;

    if (this.step === 2) {
      const data = {
        nombre: this.name,
        surname: this.surname,
      };
      this.stepData.emit(data);
    }

    if (this.step === 3) {
      this.loading = true;
      // APi
      this.apiService.payment().subscribe((res) => {
        this.loading = false;
        console.log(res);
        this.result = res;
        this.imgResult = 'assets/icons/' + this.result.img;
        this.stepData.emit(res);
      });
    }
  }

  prevStep(): void {
    this.step--;
    this.storage.pop();
  }

  controlInputs(name, surname) {
    name = name === '' ? undefined : name;
    surname = surname === '' ? undefined : surname;
    if (name === undefined && surname !== undefined) {
      this.toastrService.error('Introduzca su nombre', 'Error', {
        timeOut: 3000,
      });
      return false;
    }
    if (name !== undefined && surname === undefined) {
      this.toastrService.error('Introduzca sus apellidos', 'Error', {
        timeOut: 3000,
      });
      return false;
    }
    if (name === undefined && surname === undefined) {
      this.toastrService.error('Introduzca su nombre y apellidos', 'Error', {
        timeOut: 3000,
      });
      return false;
    }
    return true;
  }
}

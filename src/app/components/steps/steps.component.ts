import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss'],
})
export class StepsComponent implements OnInit {
  constructor() {}

  storage: Storage[] = [];

  ngOnInit(): void {}

  stepData(event) {
    // storage
    this.storage.push(event);
    console.log(this.storage);
  }
}

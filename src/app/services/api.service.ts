import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor() {}

  payment(): Observable<any> {
    return new Observable((observer) => {
      fetch('http://www.mocky.io/v2/5e3d41272d00003f7ed95c09', {
        method: 'GET',
      })
        .then((response) => response.json())
        .then((result) => {
          observer.next(result);
          observer.complete();
        })
        .catch((error) => console.log('error', error));
    });
  }
}
